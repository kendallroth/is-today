# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Fixed
- Fix Splash Screen hiding before fully loaded

## [0.3.0] - 2021-06-08
### Added
- Ability to reset date customization options (!2)

### Updated
- Switch from context to Redux for date customization options (!2)

## [0.2.0] - 2021-06-05
### Added
- Customizable day-of-week option (no longer locked to Friday!)
- Save and load selected day-of-week

## [0.1.0] - 2021-06-04
### Added
- Ability to check whether "today" is "Friday"
- Settings screen (non-functional)
- About screen with developer information
