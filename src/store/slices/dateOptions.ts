import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
import { RootState } from "@store";
import { DateOptions, DateOptionType } from "@typings/date-customization";

/** Initial customizable date options */
const initialDateOptions: DateOptions = {
  weekday: {
    name: "Weekday",
    order: 1,
    type: DateOptionType.WEEKDAY,
    value: "Friday",
  },
  day: {
    name: "Day",
    order: 2,
    type: DateOptionType.DAY,
    value: null,
  },
  month: {
    name: "Month",
    order: 3,
    type: DateOptionType.MONTH,
    value: null,
  },
};

/** Date customization slice state */
type IAppSliceState = DateOptions;

/** Payload for setting date option action */
interface ISetDateOptionPayload {
  type: DateOptionType;
  value: string | null;
}

const initialState: IAppSliceState = {
  ...initialDateOptions,
};

const dateOptionsSlice = createSlice({
  name: "dateOptions",
  initialState,
  reducers: {
    /**
     * Reset date customization options
     */
    resetOptions: () => {
      return initialState;
    },
    /**
     * Set a date customization option
     */
    setOption: (state, action: PayloadAction<ISetDateOptionPayload>) => {
      const { type, value } = action.payload;

      const existingOption = state[type];
      if (!existingOption) return;

      const newOptions = {
        ...state,
        [type]: {
          ...existingOption,
          value,
        },
      };

      // Prevent removing all date options (could also default back to initial state...)
      const hasAnyValues = Object.values(newOptions).some((option) =>
        Boolean(option.value),
      );
      if (!hasAnyValues) return;

      return newOptions;
    },
  },
});

export const selectDateOptions = (state: RootState): DateOptions =>
  state.dateOptions;

export const { resetOptions, setOption } = dateOptionsSlice.actions;

export default dateOptionsSlice.reducer;
export { initialDateOptions };
