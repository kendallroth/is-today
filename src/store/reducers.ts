import { combineReducers } from "@reduxjs/toolkit";

// Utilities
import dateOptionsReducer from "./slices/dateOptions";

const reducers = combineReducers({
  dateOptions: dateOptionsReducer,
});

export default reducers;
