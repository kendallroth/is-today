import AsyncStorage from "@react-native-async-storage/async-storage";
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import {
  persistReducer,
  persistStore,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";

// Utilities
import reducers from "./reducers";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  whitelist: ["dateOptions"],
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = configureStore({
  middleware: getDefaultMiddleware({
    serializableCheck: {
      // Ignore serialization issues caused by redux-persist actions
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }),
  reducer: persistedReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

const setupStore = () => {
  const persistor = persistStore(store);

  // @ts-ignore
  if (process.env.NODE_ENV === "development" && module.hot) {
    // @ts-ignore
    module.hot.accept("./reducers", () => {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const nextReducer = require("./reducers").default;
      store.replaceReducer(persistReducer(persistConfig, nextReducer));
    });
  }

  return { persistor, store };
};

export default setupStore;
