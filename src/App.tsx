import React from "react";
import { registerRootComponent } from "expo";
import { StatusBar } from "expo-status-bar";
import { StyleSheet, View } from "react-native";
import "react-native-gesture-handler";
import { enableScreens } from "react-native-screens";
import { Provider as PaperProvider } from "react-native-paper";
import { Provider as ReduxProvider } from "react-redux";

// NOTE: Optimize React Navigation memory usage/performance?
// Taken from: https://reactnavigation.org/docs/react-native-screens/
enableScreens();

// Components
import AppDataLoader from "@components/AppDataLoader";
import { ContextProvider } from "@context";

// Utilities
import setupStore from "@store";
import theme, { colors } from "@theme";
import Router from "./Router";

const { persistor, store } = setupStore();

const App = (): React.ReactElement | null => {
  // NOTE: Redux store provider must be outside PaperProvider!
  return (
    <ReduxProvider store={store}>
      <ContextProvider>
        <PaperProvider theme={theme}>
          {/* NOTE: Children are not rendered until app is fully loaded! */}
          <AppDataLoader persistor={persistor}>
            <View style={styles.appContainer}>
              <StatusBar style="light" />
              <Router />
            </View>
          </AppDataLoader>
        </PaperProvider>
      </ContextProvider>
    </ReduxProvider>
  );
};

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    backgroundColor: colors.primary,
  },
});

registerRootComponent(App);
