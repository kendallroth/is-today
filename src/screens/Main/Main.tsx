import React, { useCallback, useEffect, useState } from "react";
import dayjs from "dayjs";
import { StyleSheet, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { ActivityIndicator, Button } from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

// Components
import { AppBar } from "@components/layout";
import { Text } from "@components/typography";

// Utilities
import { useAppSelector } from "@hooks";
import { selectDateOptions } from "@store/slices/dateOptions";
import { colors } from "@theme";

// Types
import { DateOptionType } from "@typings/date-customization";

const answerIconSize = 64;

const MainScreen = (): React.ReactElement => {
  const dateOptions = useAppSelector(selectDateOptions);
  const navigation = useNavigation();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    checkDay();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /**
   * Simulate "loading" effect when "checking" day
   */
  const checkDay = useCallback(() => {
    setLoading(true);

    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);

  const weekdayValue = dateOptions[DateOptionType.WEEKDAY].value;

  const dayString = dayjs().format("dddd");
  const checkString = weekdayValue;
  const isDay = dayString === checkString;
  const answerText = isDay ? "Yup!" : "Nope!";

  /** Go to Settings page */
  const onSettingsPress = () => {
    navigation.navigate("Settings");
  };

  return (
    <View style={styles.page}>
      <AppBar back={false} logo title="">
        <AppBar.Action icon="cog" onPress={onSettingsPress} />
      </AppBar>
      <View style={styles.pageContent}>
        <View style={styles.appQuestion}>
          <View style={styles.appQuestionText}>
            <Text style={styles.appQuestionTextMain}>Is Today&hellip;</Text>
            <Text style={styles.appQuestionTextDay}>{checkString}?</Text>
          </View>
          <View style={styles.appQuestionAnswer}>
            <View style={styles.appQuestionAnswerIcon}>
              {loading ? (
                <ActivityIndicator
                  color={colors.accentLight}
                  size={answerIconSize}
                />
              ) : (
                <Icon
                  color={isDay ? colors.accentLight : colors.error}
                  name={isDay ? "check-circle" : "close-circle"}
                  size={answerIconSize}
                />
              )}
            </View>
            {!loading && (
              <Text style={styles.appQuestionAnswerText}>{answerText}</Text>
            )}
          </View>
        </View>
        <Button
          color={colors.accentLight}
          disabled={loading}
          mode="text"
          style={styles.appRefreshButton}
          onPress={checkDay}
        >
          Check again
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  appQuestion: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 64, // Account for app bar in spacing (to appear centered)
  },
  appQuestionAnswer: {
    position: "relative",
    alignItems: "center",
  },
  appQuestionAnswerIcon: {
    height: answerIconSize + 4,
    width: answerIconSize + 4,
  },
  appQuestionAnswerText: {
    position: "absolute",
    bottom: -48,
    marginTop: 16,
    fontSize: 24,
    lineHeight: 28,
  },
  appQuestionText: {
    alignItems: "center",
    marginBottom: 16,
  },
  appQuestionTextDay: {
    fontSize: 36,
    lineHeight: 40,
    fontWeight: "700",
    color: colors.accentLight,
  },
  appQuestionTextMain: {
    marginBottom: 8,
    fontSize: 24,
    lineHeight: 28,
  },
  appRefreshButton: {
    marginTop: "auto",
  },
  page: {
    flexGrow: 1,
  },
  pageContent: {
    flexGrow: 1,
    padding: 24,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default MainScreen;
