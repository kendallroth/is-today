import React, { useRef, useState } from "react";
import { StyleSheet, Vibration, View } from "react-native";
import { List } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

// Components
import { ConfirmSheet, SelectSheet } from "@components/dialogs";
import { AppBar } from "@components/layout";
import { Text } from "@components/typography";
import { CustomizationListItem } from "./components";

// Utilities
import { useAppDispatch, useAppSelector } from "@hooks";
import {
  resetOptions,
  selectDateOptions,
  setOption,
} from "@store/slices/dateOptions";
import { colors } from "@styles/theme";
import config from "@utilities/config";
import { weekDayOptions } from "@utilities/options";

// Types
import { IBottomSheetRef } from "@components/dialogs";
import { DateOptionType } from "@typings/date-customization";

const SettingsScreen = (): React.ReactElement => {
  const dispatch = useAppDispatch();
  const navigation = useNavigation();
  const dateOptions = useAppSelector(selectDateOptions);
  const [selectedOption, setSelectedOption] =
    useState<DateOptionType | null>(null);
  const confirmRef = useRef<IBottomSheetRef>(null);
  const selectRef = useRef<IBottomSheetRef>(null);

  // TODO: Update once more date option types are supported
  // const options: IDateOption[] = sort(Object.values(dateOptions)).asc("order");
  const weekdayOption = dateOptions[DateOptionType.WEEKDAY];
  const weekdayValue = weekdayOption.value;

  /** Go to About page */
  const onAboutPress = () => {
    navigation.navigate("About");
  };

  /**
   * Clear a date customization option
   *
   * NOTE: Clearing the last date customization option will reset initial state!
   *
   * @param option - Cleared option
   */
  const onOptionClear = (option: DateOptionType) => {
    dispatch(
      setOption({
        type: option,
        value: null,
      }),
    );

    Vibration.vibrate(100);
  };

  /**
   * Reset selected date customization options
   */
  const onResetDateConfirm = () => {
    dispatch(resetOptions());
  };

  /**
   * Prompt to confirm date reset
   */
  const onResetDatePress = () => {
    confirmRef.current?.open();
  };

  /**
   * Prompt user to set a date customization option
   *
   * @param option - Selected option
   */
  const onOptionPress = (option: DateOptionType) => {
    setSelectedOption(option);

    selectRef.current?.open();
  };

  /**
   * Set a date customization options (from dialog)
   *
   * @param option - Date option type
   * @param value  - Date option value
   */
  const onSetOptionPress = (option: DateOptionType, value: string) => {
    dispatch(
      setOption({
        type: option,
        value,
      }),
    );

    setSelectedOption(null);
    selectRef.current?.close();
  };

  return (
    <View style={styles.page}>
      <AppBar title="Settings" />
      <View style={styles.pageContent}>
        <View style={styles.settingsList}>
          <List.Section>
            <List.Subheader>Customize Day</List.Subheader>
            <CustomizationListItem
              title="Weekday"
              value={weekdayValue}
              // TODO: Update once more date option types are supported
              onLongPress={() => onOptionClear(DateOptionType.WEEKDAY)}
              onPress={() => onOptionPress(DateOptionType.WEEKDAY)}
            />
            <List.Item
              left={(_props) => <List.Icon {..._props} icon="restore" />}
              title="Reset"
              onPress={() => null}
              onLongPress={onResetDatePress}
            />
          </List.Section>
          <List.Section>
            <List.Subheader>About</List.Subheader>
            <List.Item
              left={(_props) => (
                <List.Icon
                  {..._props}
                  color={colors.accentLight}
                  icon="information"
                />
              )}
              title="About this app"
              onPress={onAboutPress}
            />
          </List.Section>
        </View>
        <Text style={styles.settingsVersion}>v{config.version}</Text>
      </View>
      <ConfirmSheet
        ref={confirmRef}
        title="Reset date customization?"
        onConfirm={onResetDateConfirm}
      />
      <SelectSheet
        ref={selectRef}
        closeOnSelect={false}
        title={`Select ${weekdayOption.name}`}
        options={weekDayOptions}
        value={weekdayValue}
        onSelect={(val) => onSetOptionPress(DateOptionType.WEEKDAY, val)}
      />
    </View>
  );
};

const pagePadding = 24;
const styles = StyleSheet.create({
  page: {
    flexGrow: 1,
  },
  pageContent: {
    flexGrow: 1,
    padding: pagePadding,
  },
  settingsList: {
    margin: -pagePadding,
  },
  settingsVersion: {
    marginTop: "auto",
    color: colors.greyLight,
  },
});

export default SettingsScreen;
