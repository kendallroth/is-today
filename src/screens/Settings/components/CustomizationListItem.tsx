import React from "react";
import { StyleSheet, View, ViewStyle } from "react-native";
import { List } from "react-native-paper";

// Components
import { Text } from "@components/typography";

type CustomizationListItemProps = {
  /** Whether list item is disabled */
  disabled?: boolean;
  /** List item title */
  title: string;
  /** List item style */
  style?: ViewStyle;
  /** list item value (right) */
  value?: string | number | null;
  /** Long press handler */
  onLongPress?: () => void;
  /** Press handler */
  onPress?: () => void;
};

const CustomizationListItem = (
  props: CustomizationListItemProps,
): React.ReactElement => {
  const {
    disabled = false,
    title,
    style = {},
    value = null,
    onLongPress,
    onPress,
  } = props;

  return (
    <List.Item
      disabled={disabled}
      right={(rightProps) => (
        <View style={styles.listItemRight}>
          <Text {...rightProps} style={styles.listItemRightValue}>
            {value ?? "–"}
          </Text>
        </View>
      )}
      style={[styles.listItem, style]}
      title={title}
      onLongPress={onLongPress}
      onPress={onPress}
    />
  );
};

const styles = StyleSheet.create({
  listItem: {},
  listItemRight: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center",
    marginRight: 8,
  },
  listItemRightValue: {},
});

export default CustomizationListItem;
