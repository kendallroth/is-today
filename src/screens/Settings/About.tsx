import React from "react";
import dayjs from "dayjs";
import * as Linking from "expo-linking";
import { Image, ScrollView, StyleSheet, View } from "react-native";
import { Chip } from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

// Components
import { AppBar } from "@components/layout";
import { Text } from "@components/typography";

// Utilities
import config from "@config";
import { colors } from "@theme";

const { links } = config;
const developerActions = [
  { icon: "account", name: "Portfolio", url: links.developerUrl },
  { icon: "gitlab", name: "Repository", url: links.gitlabUrl },
  { icon: "email", name: "Contact", url: `mailto:${links.developerEmail}` },
];

const AboutScreen = (): React.ReactElement => {
  /** Open an external link */
  const onLink = (link: string) => {
    Linking.openURL(link);
  };

  return (
    <View style={styles.page}>
      <AppBar title="About" />
      <ScrollView contentContainerStyle={styles.pageContent}>
        <View style={styles.aboutLogo}>
          <Image
            fadeDuration={100}
            resizeMode="contain"
            source={require("@assets/icon.png")}
            style={styles.aboutLogoImage}
          />
        </View>
        <Text style={styles.aboutDescription}>
          &ldquo;Is Today?&rdquo; enables you to satisfy your burning desire to
          know whether today is, in fact, a certain day.
        </Text>
        <View style={styles.aboutDeveloper}>
          <Text style={styles.aboutDeveloperText}>
            Developed by Kendall Roth &copy; {dayjs().format("YYYY")}
          </Text>
          <View style={styles.aboutDeveloperActions}>
            {developerActions.map((action) => (
              <Chip
                key={action.name}
                icon={(iconProps) => (
                  <Icon
                    {...iconProps}
                    color={colors.accentLight}
                    name={action.icon}
                    size={20}
                  />
                )}
                style={styles.aboutDeveloperActionsChip}
                textStyle={styles.aboutDeveloperActionsText}
                onPress={() => onLink(action.url)}
              >
                {action.name}
              </Chip>
            ))}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const pagePadding = 24;
const styles = StyleSheet.create({
  aboutDescription: {
    marginBottom: 16,
    paddingVertical: 4,
    paddingLeft: 16,
    fontSize: 18,
    lineHeight: 24,
    borderLeftWidth: 4,
    borderLeftColor: `${colors.white}aa`,
    borderRadius: 4,
  },
  aboutDeveloper: {
    marginTop: "auto",
  },
  aboutDeveloperActions: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 16,
  },
  aboutDeveloperActionsChip: {
    marginBottom: 8,
    marginRight: 8,
    backgroundColor: colors.primaryLight,
  },
  aboutDeveloperActionsText: {
    fontSize: 15,
  },
  aboutDeveloperText: {
    color: colors.greyLight,
    textAlign: "center",
  },
  aboutLogo: {
    alignItems: "center",
    marginTop: -pagePadding,
    marginBottom: pagePadding / 2,
  },
  aboutLogoImage: {
    height: 128,
    width: 128,
  },
  page: {
    flexGrow: 1,
  },
  pageContent: {
    flexGrow: 1,
    padding: pagePadding,
  },
});

export default AboutScreen;
