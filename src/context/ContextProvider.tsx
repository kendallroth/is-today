/**
 * Eliminate nesting multiple context providers to the app!
 *
 * Taken from: https://tobea.dev/post/global-state-cleanliness-with-react-context#lets-get-fancy
 */

import React from "react";

type ProviderComposerProps = {
  children: React.ReactElement;
  providers: React.ReactElement[];
};

/**
 * Compose Context providers together
 * @param {Node}     children  - React children
 * @param {Object[]} providers - React context providers
 */
const ProviderComposer = ({
  children,
  providers,
}: ProviderComposerProps): any =>
  providers.reduceRight(
    (kids, parent) =>
      React.cloneElement(parent, {
        children: kids,
      }),
    children,
  );

type ContextProviderProps = {
  children: React.ReactElement;
};

/**
 * Combine Context providers into single React component
 * @param {Node}     children  - React children
 */
const ContextProvider = ({
  children,
}: ContextProviderProps): React.ReactElement => (
  <ProviderComposer providers={[]}>{children}</ProviderComposer>
);

export default ContextProvider;
