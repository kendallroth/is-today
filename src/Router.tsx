import React from "react";
import { DefaultTheme, NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

// Screens
import { MainScreen } from "@screens/Main";
import { AboutScreen, SettingsScreen } from "@screens/Settings";

// Utilities
import { colors } from "@styles/theme";

const Stack = createStackNavigator();

const navigationTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: colors.primary,
  },
};

const Router = (): React.ReactElement => (
  <NavigationContainer theme={navigationTheme}>
    <Stack.Navigator
      initialRouteName="Main"
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen component={MainScreen} name="Main" />
      <Stack.Screen component={SettingsScreen} name="Settings" />
      <Stack.Screen component={AboutScreen} name="About" />
    </Stack.Navigator>
  </NavigationContainer>
);

export default Router;
