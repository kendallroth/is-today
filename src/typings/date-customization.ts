/** Date customization option type */
export enum DateOptionType {
  DAY = "day",
  MONTH = "month",
  WEEKDAY = "weekday",
}

/** Customizable date option */
export interface IDateOption {
  /** Date option name */
  name: string;
  /** Sorting order (optional) */
  order?: number;
  /** Date option type */
  type: DateOptionType;
  /** Date option value */
  value: string | null;
}

/** Customizable date options */
export type DateOptions = Record<DateOptionType, IDateOption>;
