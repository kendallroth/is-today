import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";

// Utilities
import type { AppDispatch, RootState } from "@store";

// NOTE: Use custom hooks instead of plain `useDispatch` and `useSelector`

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
