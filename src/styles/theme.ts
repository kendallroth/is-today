import { DefaultTheme } from "react-native-paper";

/**
 * Color options
 *
 * #473989 - Purple (dark)
 * #764bdd - Purple (light)
 * #4084af - Blue (light)
 * #143261 - Blue (dark)
 * #7b043d - Maroon
 * #009688 - Teal
 */

const primary = "#473989";
const colors = {
  ...DefaultTheme.colors,
  // Theme colors
  accent: "#7a8939",
  accentLight: "#b6c053",
  background: primary,
  error: "#c62828",
  primary,
  primaryLight: "#5f55ac",
  surface: "#ffffff",
  text: "#ffffff",
  // Greyscale
  white: "#ffffff",
  black: "#000000",
  greyDark: "#546e7a",
  grey: "#78909c",
  greyLight: "#b0b3c5",
};

/** App theme */
const theme = {
  ...DefaultTheme,
  colors,
};

export { colors };
export default theme;
