import React from "react";
import { Image, StatusBar, StyleSheet, View } from "react-native";
import { Appbar as BaseAppBar } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

// Utilities
import { colors } from "@styles/theme";

type AppBarProps = {
  back?: boolean;
  children?: React.ReactNode;
  logo?: boolean;
  subtitle?: string;
  title: string;
};

const AppBar = (props: AppBarProps): React.ReactElement => {
  const { back = true, children, logo = false, subtitle, title } = props;

  const navigation = useNavigation();

  return (
    <BaseAppBar.Header
      // NOTE: Only applies on Android (iOS already handled with 'SafeAreaView')
      statusBarHeight={StatusBar.currentHeight}
      style={styles.header}
    >
      {back && <BaseAppBar.BackAction onPress={navigation.goBack} />}
      {logo && (
        <View style={styles.headerLogo}>
          <Image
            fadeDuration={100}
            resizeMode="contain"
            source={require("@assets/icon.png")}
            style={styles.headerLogoImage}
          />
        </View>
      )}
      <BaseAppBar.Content subtitle={subtitle} title={title} />
      {children}
    </BaseAppBar.Header>
  );
};

const logoSize = 32;
const styles = StyleSheet.create({
  header: {
    // NOTE: Disabling shadow is currently not necessary
    elevation: 0,
    shadowOpacity: 0,
  },
  headerLogo: {
    padding: 4,
    marginLeft: 8,
    borderRadius: logoSize,
    backgroundColor: `${colors.white}44`,
  },
  headerLogoImage: {
    position: "relative",
    top: -2,
    width: logoSize,
    height: logoSize,
  },
});

AppBar.Action = BaseAppBar.Action;

export default AppBar;
