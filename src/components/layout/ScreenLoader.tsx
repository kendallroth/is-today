import React from "react";
import { StyleSheet, View, ViewStyle } from "react-native";
import { ActivityIndicator } from "react-native-paper";

// Components
import { Text } from "@components/typography";

// Utilities
import { colors } from "@theme";

type ScreenLoaderProps = {
  text?: string;
  style?: ViewStyle;
  [key: string]: any;
};

/**
 * Screen loading indicator
 *
 * @param {string} text - Optional loading text
 */
const ScreenLoader = (props: ScreenLoaderProps): React.ReactElement => {
  const { style = {}, text = null, ...rest } = props;

  return (
    <View {...rest} style={[styles.page, style]}>
      <ActivityIndicator color={colors.accentLight} size={64} />
      {Boolean(text) && <Text style={styles.pageText}>{text}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.background,
  },
  pageText: {
    marginTop: 32,
    fontSize: 16,
    fontWeight: "700",
  },
});

export default ScreenLoader;
