import React, { forwardRef } from "react";
import { StyleSheet, View } from "react-native";
import { Button } from "react-native-paper";

// Components
import { Text } from "@components/typography";
import BottomSheet, { IBottomSheetRef } from "./BottomSheet";

// Utilities
import { colors } from "@styles/theme";

// Types
import { BottomSheetProps } from "./BottomSheet";

type _ConfirmSheetProps = {
  /** Cancel button text */
  cancelText?: string;
  /** Confirm button text */
  confirmText?: string;
  /** Whether select is disabled */
  disabled?: boolean;
  /** Whether select is loading */
  loading?: boolean;
  /** Confirmation title */
  title: string;
  /** Cancellation handler */
  onCancel?: () => void;
  /** Confirmation handler */
  onConfirm: () => void;
};

type ConfirmSheetProps = Omit<BottomSheetProps, "children"> &
  _ConfirmSheetProps;

const ConfirmSheet = forwardRef(
  (props: ConfirmSheetProps, ref: any): React.ReactElement => {
    const {
      cancelText = "No",
      confirmText = "Yes",
      disabled = false,
      loading = false,
      title,
      onCancel: onCancelProp,
      onConfirm: onConfirmProp,
    } = props;

    /** Cancellation handler */
    const onCancel = () => {
      ref.current.close();

      onCancelProp && onCancelProp();
    };

    /** Confirmation handler */
    const onConfirm = () => {
      ref.current.close();

      onConfirmProp && onConfirmProp();
    };

    return (
      <BottomSheet ref={ref} style={styles.confirmPrompt}>
        <Text style={styles.confirmPromptTitle}>{title}</Text>
        <View style={styles.confirmPromptActions}>
          <Button
            color={colors.accentLight}
            disabled={disabled || loading}
            mode="outlined"
            style={styles.confirmPromptButton}
            onPress={onCancel}
          >
            {cancelText}
          </Button>
          <Button
            color={colors.accentLight}
            disabled={disabled}
            loading={loading}
            mode="contained"
            style={[styles.confirmPromptButton, { marginLeft: 16 }]}
            onPress={onConfirm}
          >
            {confirmText}
          </Button>
        </View>
      </BottomSheet>
    );
  },
);
ConfirmSheet.displayName = "ConfirmSheet";

const styles = StyleSheet.create({
  confirmPrompt: {
    // NOTE: Separate styles necessary to override parent
    paddingTop: 28,
    paddingBottom: 32,
    paddingHorizontal: 24,
  },
  confirmPromptActions: {
    flexDirection: "row",
  },
  confirmPromptButton: {
    flexGrow: 1,
    flexShrink: 1,
  },
  confirmPromptTitle: {
    marginBottom: 24,
    fontSize: 18,
    lineHeight: 20,
    fontWeight: "700",
  },
});

export default ConfirmSheet;
export type { ConfirmSheetProps };
