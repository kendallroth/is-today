import React, { forwardRef } from "react";
import { StyleSheet, View } from "react-native";
import { ActivityIndicator, List } from "react-native-paper";

// Components
import { Text } from "@components/typography";
import BottomSheet from "./BottomSheet";

// Utilities
import { colors } from "@styles/theme";

// Types
import { BottomSheetProps } from "./BottomSheet";

type _SelectSheetProps = {
  /** Whether select should close after selection */
  closeOnSelect?: boolean;
  /** Whether select is disabled */
  disabled?: boolean;
  /** Whether select is loading */
  loading?: boolean;
  /** Selectable options */
  options: string[];
  /** Select title */
  title: string;
  /** Selected value */
  value: string | null;
  /** Select handler */
  onSelect: (option: string) => void;
};

type SelectSheetProps = Omit<BottomSheetProps, "children"> & _SelectSheetProps;

const SelectSheet = forwardRef(
  (props: SelectSheetProps, ref): React.ReactElement => {
    const {
      closeOnSelect = true,
      disabled = false,
      loading = false,
      options = [],
      title,
      value,
      onSelect,
      ...rest
    } = props;

    /**
     * Select an option
     *
     * @param value - Selected option
     */
    const onOptionPress = (option: string) => {
      if (disabled || loading) return;

      onSelect(option);

      if (closeOnSelect) {
        close();
      }
    };

    return (
      <BottomSheet {...rest} ref={ref}>
        <View style={styles.modalContentHeader}>
          <Text style={styles.modalContentHeaderText}>{title}</Text>
          {Boolean(loading) && (
            <ActivityIndicator
              color={colors.accentLight}
              style={styles.modalContentHeaderLoading}
            />
          )}
        </View>
        <View style={styles.modalContentList}>
          {options.map((option) => {
            const selected = option === value;
            const titleStyle = selected
              ? { color: colors.accentLight, fontWeight: "700" }
              : {};

            if (disabled || loading) {
              titleStyle.color = colors.greyLight;
            }

            return (
              <List.Item
                key={option}
                disabled={disabled || loading}
                right={
                  selected
                    ? (_props) => (<List.Icon {..._props} color={colors.accentLight} icon="check" />) // prettier-ignore
                    : () => null
                }
                title={option}
                titleStyle={titleStyle}
                onPress={() => onOptionPress(option)}
              />
            );
          })}
        </View>
      </BottomSheet>
    );
  },
);

const modalPadding = 16;
const styles = StyleSheet.create({
  modalContentHeader: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 8,
  },
  modalContentHeaderLoading: {
    marginLeft: "auto",
  },
  modalContentHeaderText: {
    fontSize: 18,
    lineHeight: 22,
    fontWeight: "700",
  },
  modalContentList: {
    marginHorizontal: -modalPadding,
  },
});

export default SelectSheet;
export type { SelectSheetProps };
