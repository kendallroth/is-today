import React, { forwardRef, useImperativeHandle, useState } from "react";
import { StyleSheet, View, ViewStyle } from "react-native";
import Modal from "react-native-modal";

// Utilities
import { colors } from "@styles/theme";

interface IBottomSheetRef {
  close: () => void;
  open: () => void;
}

type BottomSheetProps = {
  /** Bottom sheet content */
  children: React.ReactElement | React.ReactElement[];
  /** Bottom sheet style */
  style?: ViewStyle;
  /** Back handler */
  onClose?: () => void;
};

const BottomSheet = forwardRef(
  (props: BottomSheetProps, ref): React.ReactElement => {
    const { children, style = {}, onClose } = props;

    const [isOpen, setIsOpen] = useState(false);

    useImperativeHandle(ref, (): IBottomSheetRef => {
      return {
        close,
        open,
      };
    });

    /** Close the modal (from external source) */
    const close = () => {
      if (!isOpen) return;
      setIsOpen(false);

      // Alert parent that bottom sheet has closed (only for internal closures)!
      onClose && onClose();
    };

    /** Open the modal */
    const open = () => {
      if (isOpen) return;
      setIsOpen(true);
    };

    return (
      <Modal
        // NOTE: Necessary to fix backdrop flicker bug when closing.
        backdropTransitionOutTiming={0}
        isVisible={isOpen}
        style={styles.modal}
        useNativeDriver
        onBackButtonPress={close}
        onBackdropPress={close}
      >
        <View style={[styles.modalContent, style]}>{children}</View>
      </Modal>
    );
  },
);
BottomSheet.displayName = "BottomSheet";

const modalPadding = 16;
const styles = StyleSheet.create({
  modal: {
    justifyContent: "flex-end",
    margin: 0,
  },
  modalContent: {
    padding: modalPadding,
    backgroundColor: colors.primary,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
});

export default BottomSheet;
export type { BottomSheetProps, IBottomSheetRef };
