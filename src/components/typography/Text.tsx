import React, { useMemo } from "react";
import { StyleSheet } from "react-native";
import { Text as RNPText } from "react-native-paper";

// Utilities
import { colors } from "@theme";

type TextProps = {
  align?: "left" | "center" | "right";
  children: React.ReactNode;
  color?: string;
  size?: number;
  style?: Record<string, any>;
  [key: string]: any;
};

/**
 * Text component
 *
 * @param {string} align    - Text alignment
 * @param {string} children - Text
 * @param {string} color    - Text color
 * @param {number} size     - Font size
 * @param {object} style    - Text style
 */
const Text = (props: TextProps): React.ReactElement => {
  const {
    align = "left",
    children = null,
    color = colors.white,
    size = 16,
    style = {},
    ...rest
  } = props;

  const styleArray = useMemo(
    () => (Array.isArray(style) ? style : [style]),
    [style],
  );
  const textStyle = useMemo(() => {
    return {
      color,
      fontSize: size,
      textAlign: align,
    };
  }, [align, color, size]);

  return (
    <RNPText {...rest} style={[styles.text, textStyle, ...styleArray]}>
      {children}
    </RNPText>
  );
};

const styles = StyleSheet.create({
  text: {
    lineHeight: 22,
  },
});

export default Text;
