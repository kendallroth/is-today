import AsyncStorage from "@react-native-async-storage/async-storage";

/**
 * Storage interface for AsyncStorage
 */
class StorageService {
  /**
   * Get a JSON value
   *
   * @param   key - Item key
   * @returns - Item value (parsed)
   */
  async getJson(key: string): Promise<Record<string, any> | null> {
    const value = await AsyncStorage.getItem(key);
    if (!value) return null;

    return JSON.parse(value);
  }

  /**
   * Get a string value
   *
   * @param   key - Item key
   * @returns - Item value (simple)
   */
  async getString(key: string): Promise<string | null> {
    return AsyncStorage.getItem(key);
  }

  /**
   * Store a JSON value
   *
   * @param key   - Item key
   * @param value - Item value
   */
  async setJson(key: string, value: Record<string, any>) {
    return AsyncStorage.setItem(key, JSON.stringify(value));
  }

  /**
   * Store an simple value
   *
   * @param key   - Item key
   * @param value - Item value
   */
  async setString(key: string, value: any) {
    return AsyncStorage.setItem(key, value);
  }
}

const storageService = new StorageService();
export default storageService;
