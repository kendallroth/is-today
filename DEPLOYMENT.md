# Deployment

Expo handles much of the building process; however, the resulting bundles must be uploaded to Google Play and TestFlight manually.

> **NOTE:** Manual updates are only required when changing native code (or app configuration); however, it is recommended anyway as over-the-air updates are not automatically applied to new downloads!

## Store Setup

### Android

_TBD_

### iOS

_TBD_

## Building App

Building a new app release is very similar across platforms (once the initial release has been created).

1. Update descriptive version (`package.json`)
2. Update incremental Android `versionCode` (_Android only_)
3. Update `CHANGELOG` if changes are missing
4. Build app with `expo build:android -t app-bundle` or `expo build:ios -t archive`
5. Upload binary to target app store when complete

> **NOTE:** Android and iOS support a version system like `<major>.<minor>.<revision>` for their description versions; however, Android requires an incremental `versionCode` for their internal versioning (iOS can use same version for build code).
