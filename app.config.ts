// Utilities
import { version } from "./package.json";

const primaryColor = "#473989";

export default {
  // Information
  name: "Is Today?",
  description: "Check whether today is a given day",
  slug: "is-today",
  version, // Android 'versionName', iOS 'CFBundleShortVersionString'
  privacy: "public",
  platforms: ["android", "ios"],
  orientation: "portrait",
  entryPoint: "./src/App.tsx",

  // Theme
  backgroundColor: primaryColor,
  icon: "./assets/icon.png",
  primaryColor, // Currently Android only (multi-tasker)
  splash: {
    image: "./assets/splash.png",
    resizeMode: "contain",
    backgroundColor: primaryColor,
  },

  updates: {
    fallbackToCacheTimeout: 0,
  },
  assetBundlePatterns: ["**/*"],

  // Android overrides
  android: {
    adaptiveIcon: {
      foregroundImage: "./assets/icon.png",
      backgroundColor: primaryColor,
    },
    package: "ca.kendallroth.istoday",
    permissions: [],
    versionCode: 2,
  },
  androidStatusBar: {
    barStyle: "light-content",
  },

  // iOS overrides
  ios: {
    bundleIdentifier: "ca.kendallroth.istoday",
    buildNumber: version,
    icon: "./assets/icon_solid.png",
    supportsTablet: true,
  },
};
